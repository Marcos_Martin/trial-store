class CreateDiscounts < ActiveRecord::Migration[6.0]
  def change
    create_table :discounts do |t|
      t.string :name
      t.string :product_id, foreign_key: true
      t.timestamps
    end
  end
end
