Rails.application.routes.draw do
  root to: 'home#index'
  post 'cart/add'
  delete 'cart/remove'
end
