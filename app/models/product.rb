class Product < ApplicationRecord
  self.primary_key = :code
  has_many :discounts
end
