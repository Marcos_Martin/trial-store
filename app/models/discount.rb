class Discount < ApplicationRecord
  belongs_to :product, foreign_key: :product_id, primary_key: :code
  validates_uniqueness_of :name

  def self.two_for_one(n_items, product)
    price = product.price
    total =
      if n_items > 1
        n_items.even? ? price * n_items / 2 : ((price * (n_items - 1) / 2)) + price
      else
        price
      end
    total
  end

  def self.strawberries(n_items, product)
    price = product.price
    price = 4.5 if n_items >= 3
    price * n_items
  end

  def self.only_sum(n_items, product)
    price = product.price
    price * n_items
  end
end
