class Checkout
  def initialize(pricing_rules)
    @items = []
    @pricing_rules = pricing_rules
  end

  def scan(item)
    # item = model Product
    @items << item
  end

  def total
    total = 0
    count_items.each do |item|
      code = item.keys.first.to_s # code of the product
      n_items = item.values.first # n items of this product
      product = Product.where(code: code).first
      discount = @pricing_rules.where(product: product).first # product.discounts.first
      total +=
        if discount.present?
          case discount.name
          when 'two for one'
            Discount.two_for_one(n_items, product)
          when 'strawberries'
            Discount.strawberries(n_items, product)
          end
        else
          Discount.only_sum(n_items, product)
        end
    end
    total
  end

  private

  def count_items
    counted = []
    @items.uniq.each do|item|
      n_items = @items.count(item)
      counted << { "#{item}": n_items }
    end
    counted
  end
end
