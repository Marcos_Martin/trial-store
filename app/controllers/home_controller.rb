class HomeController < ApplicationController
  def index
    @products = Product.all
    @cart = session[:cart].join(', ')
  end
end
