class CartController < ApplicationController
  def add
    item = request.params[:code]
    session[:cart] << item
    @cart = session[:cart].join(', ')
    @price = price_cart(session[:cart])
    respond_to do |format|
      format.js
    end
  end

  def remove
    session[:cart] = []
    redirect_to root_path
  end

  private

  def price_cart(cart)
    pricing_rules = Discount.all
    co = Checkout.new(pricing_rules)
    # much better: co = Checkout.new(Discount.all)
    cart.each do |item|
      co.scan(item)
    end
    price = co.total # is enough write co.total
  end
end
