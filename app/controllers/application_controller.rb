class ApplicationController < ActionController::Base
  before_action :set_defaults

  def set_defaults
  session[:cart] = [] unless session[:cart].present?
  end
end
