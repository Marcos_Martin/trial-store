class Product
  def initialize(options = {})
    @code = options[:code]
    @name = options[:name]
    @price = options[:price]
    @discount = options[:discount]
  end
  attr_reader :code, :name, :price, :discount
end