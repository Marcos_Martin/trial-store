require './libs/product'
require './libs/discount'

class Checkout
  def initialize(pricing_rules)
    @items = []
    @pricing_rules = pricing_rules
  end

  def scan(item)
    # item = model Product
    @items << item
  end

  def total
    total = 0
    discount_to_apply = Discount.new
    count_items.each do |item|
      n_items = item.values.first # n items of this product
      code = item.keys.first.to_s # product code
      product = @items.select { |i| i.code.eql?(code) }.first
      discount =
        (product.discount if @pricing_rules.include?(product.discount))
      total +=
        case discount
        when 'two_for_one'
          discount_to_apply.two_for_one(n_items, product)
        when 'strawberries'
          discount_to_apply.strawberries(n_items, product)
        when 'only_sum'
          discount_to_apply.only_sum(n_items, product)
        end
    end
    total
  end

  private

  def count_items
    counted = []
    @items.uniq.each do|item|
      n_items = @items.count(item)
      counted << { "#{item.code}": n_items }
    end
    counted
  end

end
