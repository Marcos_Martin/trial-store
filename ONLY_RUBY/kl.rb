require './libs/product'
require './libs/discount'
require './libs/checkout'

def initialize
  @products = []
  @products << Product.new(code: 'GR1',  name: 'Green tea',
                           price: 3.11,  discount: 'two_for_one')
  @products << Product.new(code: 'SR1',  name: 'Strawberries',
                           price: 5.00,  discount: 'strawberries')
  @products << Product.new(code: 'CF1',  name: 'Coffee',
                           price: 11.23, discount: 'only_sum')
  @cart = []
  @total = 0
end

def menu
  puts `clear`
  puts 'Choose a option (type a number and press Enter)'
  @products.each_with_index do |product, index|
    puts "#{index}: Add product: #{product.name} (#{product.code}, #{product.price})"
  end
  puts "#{@products.count + 1}: Destroy basket"
  puts "#{@products.count + 2}: Exit\n\n"
  puts "Basket: #{@cart.join(',')}"
  puts "Total price expected: £#{@total}"

end

def number? string
  true if Float(string)
rescue StandardError => e
  false
end

def price_cart(cart)
  pricing_rules = (Discount.instance_methods - Discount.methods).map(&:to_s)
  co = Checkout.new(pricing_rules)
  cart.each do |item|
    co.scan(item)
  end
  price = co.total # is enough write co.total
end

def transform_cart_in_model
  solve = []
  @cart.each do |cart|
    solve << @products.select { |p| p.code.eql?(cart) }.first
  end
  solve
end

# START PROGRAM
initialize
menu
while user_input = gets.chomp # loop while getting user input
  if number?(user_input) && user_input.to_i >= 0 && user_input.to_i <= @products.count + 2
    if user_input.eql?((@products.count + 2).to_s)
      # exit program
      puts '(ʘ‿ʘ)╯ bye bye'
      break
    elsif  user_input.eql?((@products.count + 1).to_s)
      # erase cart
      @cart = []
      @total = 0
      menu
    elsif user_input.to_i >= 0 && user_input.to_i <= @products.count
      # is a product to process
      @cart << @products[user_input.to_i].code
      @total = price_cart(transform_cart_in_model)
      menu
    end
  end
end