# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
require 'rails_helper'
require 'rspec-rails'

RSpec.configure do |config|
  # Verbose mode running tests
  config.formatter = :documentation
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.before(:suite) do
    # initialize DB
    tables = ActiveRecord::Base.connection.tables
    tables.each do |table_name|
      ActiveRecord::Base.connection.execute("Delete from #{table_name}")
      ActiveRecord::Base.connection.execute("DELETE FROM SQLITE_SEQUENCE WHERE name='#{table_name}'")
    end
    Rails.application.load_seed # loading seeds
  end
end
