require 'spec_helper'

RSpec.describe Checkout, type: :model do

  before do
    @pricing_rules = Discount.all
  end

  describe 'Test examples' do
    it 'GR1,SR1,GR1,GR1,CF1 = 22.45' do
      pricing_rules = @pricing_rules
      co = Checkout.new(pricing_rules)
      co.scan('GR1')
      co.scan('SR1')
      co.scan('GR1')
      co.scan('GR1')
      co.scan('CF1')
      price = co.total
      expect(price).to eq 22.45
    end
    it 'GR1,GR1 = 3.11' do
      pricing_rules = @pricing_rules
      co = Checkout.new(pricing_rules)
      co.scan('GR1')
      co.scan('GR1')
      price = co.total
      expect(price).to eq 3.11
    end
    it 'SR1,SR1,GR1,SR1 = 16.61' do
      pricing_rules = @pricing_rules
      co = Checkout.new(pricing_rules)
      co.scan('SR1')
      co.scan('SR1')
      co.scan('GR1')
      co.scan('SR1')
      price = co.total
      expect(price).to eq 16.61
    end
  end
end