require 'spec_helper'

RSpec.describe Discount, type: :model do

  before do

  end

  it { should belong_to(:product) }
  it { should validate_uniqueness_of(:name).ignoring_case_sensitivity }
end