require 'spec_helper'

RSpec.describe CartController, type: :controller do
  before(:each) do

  end

  describe 'POST #add' do
    it 'response 200' do
      params = { code: Product.all.sample.code }
      response = post :add, params: params, format: :js
      expect(response.status).to eq 200
    end
    it 'added one item' do
      params = { code: Product.all.sample.code }
      post :add, params: params, format: :js
      expect(session[:cart].count).to eq 1
    end
  end

  describe 'DELETE #REMOVE' do
    it 'erased cart' do
      delete :remove
      expect(session[:cart].present?).to be false
    end
  end
end